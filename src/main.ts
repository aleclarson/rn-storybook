import '@storybook/addon-ondevice-knobs/register';
import '@storybook/addon-ondevice-actions/register';

import { addDecorator } from '@storybook/react-native';
import { withKnobs } from '@storybook/addon-knobs';

// Allow knobs in all stories.
addDecorator(withKnobs);

export * from '@storybook/addon-knobs';
export * from '@storybook/react-native';
