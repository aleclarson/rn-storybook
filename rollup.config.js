import { dirname, resolve, sep } from 'path';
import importFrom from 'import-from';
import sortObject from 'sort-object';
import fs from 'fs';

import nodeResolve from '@rollup/plugin-node-resolve';
import babel from '@rollup/plugin-babel';
import dts from 'rollup-plugin-dts';
import jsx from './plugins/jsx';

const storybookRoot = resolve('../../..') + sep;
const storybookRE = /^(@storybook\/[^/]+)(?:\/(.+))?/;

/**
 * @returns {import('rollup').RollupOptions}
 */
const bundle = (format, plugins) => ({
  input: require('./package.json').main,
  output: {
    file: 'dist/index.' + (format == 'dts' ? 'd.ts' : 'js'),
    format: format == 'cjs' ? 'cjs' : 'es',
    sourcemap: false,
  },
  plugins,
  external(id) {
    return (
      !/^[./]/.test(id) &&
      !storybookRE.test(id) &&
      !id.startsWith(storybookRoot) &&
      // This package is forked locally.
      id !== 'react-native-switch'
    );
  },
  treeshake: {
    moduleSideEffects: 'no-external',
  },
});

const extensions = ['.js', '.ts', '.tsx'];

export default [
  bundle('esm', [
    rewireStorybookModules(),
    nodeResolve({ extensions }),
    babel({
      extensions,
      babelHelpers: 'inline',
      presets: ['@babel/preset-typescript'],
      plugins: [
        '@babel/plugin-syntax-jsx',
        ['@babel/plugin-proposal-class-properties', { loose: true }],
        ['@babel/plugin-transform-classes', { loose: true }],
      ],
    }),
    jsx(),
    makeDistPackage(),
  ]),
  bundle('dts', [dts({ respectExternal: true })]),
];

/**
 * Rewire packages to use their "src" folder.
 * The `regex` must have 2 capture groups, the 1st for the package name
 * and the 2nd for the relative name (which may not exist).
 *
 * @returns {import('rollup').Plugin}
 */
function rewireStorybookModules() {
  const resolveCache = {};
  return {
    async resolveId(id, importer) {
      if (!importer) {
        return;
      }
      const match = storybookRE.exec(id);
      if (match) {
        if (id.endsWith('/register')) {
          return;
        }

        const originalId = id;
        if (resolveCache[originalId]) {
          return resolveCache[originalId];
        }

        if (match[2]) {
          id = match[1] + '/src/' + match[2];
        } else {
          const pkg = importFrom(dirname(importer), match[1] + '/package.json');
          id = match[1] + '/' + pkg.main.replace('dist/', 'src/').replace(/\.js$/, '');
        }

        const resolved = await this.resolve(id, importer, { skipSelf: true });
        return (resolveCache[originalId] = resolved.id);
      }
      if (importer.endsWith(sep + 'register.js')) {
        id = id.replace(/\bdist\b/, 'src');

        const resolved = await this.resolve(id, importer, { skipSelf: true });
        return resolved.id;
      }
    },
  };
}

/**
 * @returns {import('rollup').Plugin}
 */
function makeDistPackage() {
  const { name, version, peerDependencies } = require('./package.json');
  return {
    renderChunk(_, chunk) {
      const dependencies = {
        // This would be a peer dependency otherwise.
        '@emotion/core': '10.0.27',
      };
      for (const importId of chunk.imports) {
        // Convert "lodash/debounce" to "lodash"
        const packageId = stripRelative(importId);
        // Add it to our dist dependencies if needed.
        if (!dependencies[packageId] && !peerDependencies[packageId]) {
          const { importers } = this.getModuleInfo(importId);
          const imported = importFrom(importers[0], packageId + '/package.json');
          dependencies[packageId] = imported.version;
          // Throw if a peer dependency is missing.
          if (imported.peerDependencies)
            Object.entries(imported.peerDependencies).forEach(([name, version]) => {
              if (!peerDependencies[name] && !dependencies[name]) {
                throw Error(`Missing peer dependency: ${name} ${version}`);
              }
            });
        }
      }
      const distPkg = {
        name,
        version,
        license: 'MIT',
        repository: 'https://bitbucket.org/aleclarson/rn-storybook',
        peerDependencies,
        dependencies: sortObject(dependencies),
      };
      this.emitFile({
        type: 'asset',
        fileName: 'package.json',
        source: JSON.stringify(distPkg, null, 2),
      });
      this.emitFile({
        type: 'asset',
        fileName: 'README.md',
        source: fs.readFileSync('README.md'),
      });
    },
  };
}

function stripRelative(moduleId) {
  return /^(@[^/]+\/)?[^/]+/.exec(moduleId)[0];
}
