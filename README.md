# @alloc/rn-storybook

Storybook for React Native, bundled with Rollup.

&nbsp;

### Why do this?

I needed to fix `@types/node` from leaking into my React Native project,
since I co-locate stories in `__stories__` directories. I also needed an 
easy way to replace the `react-native` dependency with a local fork, and 
bundling makes that much easier. Lastly, this lets me avoid publishing every 
`@storybook/*` package separately.

&nbsp;

**Other features:**
- it re-exports `@storybook/addon-knobs` for convenience
- it registers `ondevice-actions` and `ondevice-knobs` for you
- it allows every story to contain knobs by default
- it works with `@alloc/react-native-macos`

&nbsp;

*Storybook is bundled from [this fork](https://github.com/aleclarson/storybook).*
